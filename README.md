DBViz
=====

Program for visualizing [Persistent](https://hackage.haskell.org/package/persistent) table definitions.

Usage
=====

 - compile program: `stack build`
 - run binary in same folder as your `model` file is (currently there are no command line options)
 - run graphviz: `dot test.gv -Tsvg -ooutput.svg`
 - your model should now have been visualized as `output.svg`

Released under MIT license
