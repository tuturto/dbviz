{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module Lib
    ( mainFunc
    ) 
where

import ClassyPrelude
import Control.Lens ( to, (^.) )
import Data
import Data.Text.Encoding as E
import GHC.IO.Handle 
import Graph ( writeOutputData )
import Parser ( parseTables )


-- | Read command line options
readCommandLineOptions :: IO Options
readCommandLineOptions = do
    return Options 
        { _optionsInputFile = "model"
        , _optionsOutputFile = Just "output.gv"
        }


-- | Read input file
readInputData :: Options -> IO String
readInputData ops = do
    i <- readFile $ ops ^. optionsInputFile . to unpack
    return $ unpack $ E.decodeUtf8 i


-- | Main function of the program
mainFunc :: IO ()
mainFunc = do
    ops <- readCommandLineOptions
    input <- readInputData ops
    let model = parseTables input
    case model of
        Left err ->
            print err

        Right tables -> do
            writeOutputData ops tables
