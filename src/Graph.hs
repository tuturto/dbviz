{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module Graph
    ( writeOutputData
    )
where

import ClassyPrelude
import Control.Lens ( (^.), (^?), (^..), _Just, to, _1, ix, folded )
import Data
    ( TableDef(_tableDefName),
      ColumnDef(..),
      TypeName(MkTypeName),
      Comment(MkComment),
      ColumnName(MkColumnName),
      TableName(..),
      Options,
      unTableName,
      unComment,
      unColumnName,
      unTypeName,
      columnDefComment,
      columnDefName,
      columnDefType,
      tableDefColumns,
      tableDefName,
      optionsOutputFile )
import Data.Text ( replace, splitOn )


-- | Write output data into file or print it on screen
writeOutputData :: Options -> [ TableDef ] -> IO ()
writeOutputData ops tables = do
    case ops ^? optionsOutputFile . _Just . to unpack of
        Just fileName -> do
            writeFileUtf8 fileName (graph tables)

        Nothing ->
            print $ graph tables


-- | Turn list of TableDef into dot language string
graph :: [ TableDef ] -> Text
graph tables =
    header
    ++ "\n"
    ++ concat (node <$> tables)
    ++ "\n"
    ++ concat (edges tables <$> tables)
    ++ "\n"
    ++ footer


-- | Header of the dot file
header :: Text
header = "digraph db {\nlayout=dot\n"


-- | Footer of the dot file
footer :: Text
footer = "overlap=false\nfontsize=12;\n}\n"


-- | Map one TableDef into node for dot
node :: TableDef -> Text
node table =
    "\"" ++ table ^. tableDefName . unTableName ++ "\""
        ++ "[ shape = \"Mrecord\" label =<<table border=\"0\" cellborder=\"0\" cellpadding=\"3\"><tr><td colspan=\"3\">" ++ table ^. tableDefName . unTableName ++ "</td></tr>"
        -- add id column based on the table name
        ++ column ( ColumnDef { _columnDefName = MkColumnName "id"
                              , _columnDefType = (MkTypeName . (++ "Id") ._unTableName . _tableDefName) table
                              , _columnDefComment = Just $ MkComment "primary key"
                              } )
        ++ concat (column <$> table ^. tableDefColumns)
        ++ "</table>> ];\n"


-- | Map ColumnDef to matchign row in node
column :: ColumnDef -> Text
column c =
    "<tr>"
    ++ "<td align=\"left\">" ++ c ^. columnDefName . unColumnName ++ "</td>"
    ++ "<td align=\"left\">" ++ c ^. columnDefType . unTypeName ++ "</td>"
    ++ "<td align=\"left\">" ++ fromMaybe "" (c ^? columnDefComment . _Just . unComment . to (replace "\n" "<br align=\"left\" />")) ++ "</td>"
    ++ "</tr>"


-- | Create edges for single TableDef
-- | These are primary keys that link to other tables
-- | Filter out edges that don't connect to an existing table
edges :: [ TableDef ] -> TableDef -> Text
edges tables table =
    concat $ (\x -> table ^. tableDefName . unTableName ++ " -> " ++ x ^. unTableName ++ "\n") <$> parents
    where
        parents = catMaybes $ table ^.. tableDefColumns . folded . columnDefType . to (edge tables)
       

-- | Create single edge based on the field type
-- | Returns Nothing if the field is not a foreign key
-- | or if the foreign key would't connect to an existing table
edge :: [ TableDef ] -> TypeName -> Maybe TableName
edge tables tn =
    case candidate of
        Nothing ->
            Nothing

        Just tName ->
            -- filter out fields ending in "Id", but without matching table
            if any (\table -> table ^. tableDefName == tName) tables
                then Just tName
                else Nothing
    where
        pieces = tn ^. unTypeName . to words
        candidate = pieces ^? ix 0 . to (stripSuffix "Id") . _Just . to MkTableName