{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE OverloadedStrings          #-}


module Data
    ( TableName(..), ColumnName(..), IndexName(..)
    , TableDef(..), ColumnDef(..), IndexDef(..)
    , Comment(..)
    , TypeName(..), TypeClassName(..)
    , Options(..)
    , tableDefName, tableDefColumns, tableDefComment, tableDefIndices
    , columnDefName, columnDefType, columnDefComment
    , indexDefName, indexDefColumns
    , unTableName, unColumnName, unComment, unTypeName, unTypeClassName
    , optionsInputFile, optionsOutputFile
    ) 
where

import ClassyPrelude
import Control.Lens ( makeLenses, makeWrapped )

data Options = Options
    { _optionsInputFile  :: Text
    , _optionsOutputFile :: Maybe Text
    } deriving (Show, Read, Eq)

newtype TableName = MkTableName { _unTableName :: Text }
    deriving (Show, Read, Eq)


newtype ColumnName = MkColumnName { _unColumnName :: Text }
    deriving (Show, Read, Eq)


newtype Comment = MkComment { _unComment :: Text }
    deriving (Show, Read, Eq)


-- | semigroup instance of comment has special rules for <>:
-- | two empty comments results an empty comment
-- | if one of the parameters is an empty comment, result is the other parameter
-- | otherwise, result is both comments joined together, separated with a newline
instance Semigroup Comment where
    c1 <> c2 = 
        case (c1, c2) of
            (MkComment "", MkComment "") -> 
                MkComment ""

            (_, MkComment "") ->
                c1

            (MkComment "", _) ->
                c2

            (MkComment s1, MkComment s2) ->
                MkComment $ s1 <> "\n" <> s2


instance Monoid Comment where
    mempty = MkComment ""


newtype TypeName = MkTypeName { _unTypeName :: Text }
    deriving (Show, Read, Eq)


newtype TypeClassName = MkTypeClassName { _unTypeClassName :: Text }
    deriving (Show, Read, Eq)


newtype IndexName = MkIndexName { _unIndexName :: Text }
    deriving (Show, Read, Eq)


data ColumnDef = ColumnDef
    { _columnDefName    :: ColumnName
    , _columnDefType    :: TypeName
    , _columnDefComment :: Maybe Comment
    }
    deriving (Show, Read, Eq)


data IndexDef = IndexDef
    { _indexDefName :: IndexName
    , _indexDefColumns   :: [ ColumnName ]
    }
    deriving (Show, Read, Eq)


data TableDef = TableDef
    { _tableDefName     :: TableName
    , _tableDefColumns  :: [ ColumnDef ]
    , _tableDefComment  :: Maybe Comment
    , _tableDefIndices  :: [ IndexDef ]
    , _tableDefDeriving :: [ TypeClassName ]
    }
    deriving (Show, Read, Eq)


makeWrapped ''TableName
makeLenses ''TableName
makeWrapped ''Comment
makeLenses ''Comment
makeWrapped ''ColumnName
makeLenses ''ColumnName
makeWrapped ''TypeName
makeLenses ''TypeName
makeWrapped ''TypeClassName
makeLenses ''TypeClassName
makeWrapped ''IndexName
makeLenses ''IndexName

makeLenses ''ColumnDef
makeLenses ''TableDef
makeLenses ''IndexDef

makeLenses ''Options
