{-# LANGUAGE NoImplicitPrelude          #-}
{-# LANGUAGE OverloadedStrings          #-}

module Parser
    ( parseTables
    )
where

import ClassyPrelude hiding (many, try, optional, (<|>))
import Control.Lens ( (^..), folded )
import Data
    ( TableDef(..),
      IndexDef(..),
      ColumnDef(..),
      IndexName(MkIndexName),
      TypeClassName(MkTypeClassName),
      TypeName(MkTypeName),
      Comment(MkComment),
      ColumnName(MkColumnName),
      TableName(MkTableName),
      unComment )
import Text.Parsec
    ( ParseError, ParsecT(..)
    , char, parse, oneOf, newline, string, many, anyChar, noneOf, many1
    , optionMaybe, option, try, skipMany, optional, lower, upper
    , spaces, newline, notFollowedBy, choice, (<|>), (<?>)
    )
import Text.Parsec.String ( Parser )
import Text.Parsec.Combinator ( endBy )


-- | Consume whitespace
whitespace :: Parser ()
whitespace = void $ many $ oneOf " \t"


indentation :: Parser ()
indentation = void $ many1 $ oneOf " \t"


commentLine :: Parser Comment
commentLine = do
    void $ string "-- | "
    commentLine <- many $ noneOf "\n"
    newline
    return $ MkComment $ pack commentLine


tableComment :: Parser Comment
tableComment = do
    comments <- many1 commentLine
    return $ mconcat comments


columnComment :: Parser Comment
columnComment = do
    comments <- many1 (try (indentation *> commentLine))
    return $ mconcat comments


tableName :: Parser TableName
tableName = do
    void $ optional $ char '+'
    tName <- many1 $ noneOf " \t\n"
    whitespace
    return $ MkTableName $ pack tName


json :: Parser Bool
json = do
    jsonText <- option "" (string "json")
    whitespace
    let isJson = case length jsonText of
                    0 ->
                        False

                    _ ->
                        True
    return isJson


tableColumn :: Parser ColumnDef
tableColumn = do
    cComment <- optionMaybe (try columnComment)
    cName <- try (indentation *> columnName)
    cType <- columnType
    return $ ColumnDef { _columnDefName = cName
                       , _columnDefType = cType
                       , _columnDefComment = cComment
                       }


columnName :: Parser ColumnName
columnName = do
    void $ try ( do { notFollowedBy $ string "deriving" })
    fLetter <- lower
    letters <- many1 $ noneOf " \t\n"
    whitespace
    return $ MkColumnName $ pack $ fLetter : letters


columnType :: Parser TypeName
columnType = do
    fLetter <- try upper <|> try (char '(') <|> try (char '[')
    letters <- many1 $ noneOf "\n"
    newline
    return $ MkTypeName $ pack $ fLetter : letters


tableIndex :: Parser IndexDef
tableIndex = do
    iName <- try (indentation *> indexName)
    cNames <- many1 indexColumn
    whitespace
    newline
    return $ IndexDef { _indexDefName = iName
                      , _indexDefColumns = cNames
                      }


indexName :: Parser IndexName
indexName = do
    fLetter <- upper
    letters <- many1 $ noneOf " \t\n"
    whitespace
    return $ MkIndexName $ pack $ fLetter : letters


indexColumn :: Parser ColumnName
indexColumn = do
    fLetter <- lower
    letters <- many1 $ noneOf " \t\n"
    void $ many $ oneOf " \t"
    return $ MkColumnName $ pack $ fLetter : letters


tableDeriving :: Parser [ TypeClassName ]
tableDeriving = do
    indentation
    void $ string "deriving"
    void $ many1 $ oneOf " \t"
    instances <- many1 typeClassName
    newline
    return instances


typeClassName :: Parser TypeClassName
typeClassName = do
    fLetter <- upper
    letters <- many1 $ noneOf " \t\n"
    whitespace
    return $ MkTypeClassName $ pack $ fLetter : letters


table :: Parser TableDef
table = do
    cBlock <- optionMaybe tableComment
    tName <- tableName
    hasJson <- json
    newline
    columns <- many1 tableColumn
    indexDefs <- many tableIndex
    deriv <- optionMaybe tableDeriving
    whitespace
    void $ optional newline
    return $ TableDef { _tableDefName     = tName
                      , _tableDefColumns  = columns
                      , _tableDefComment  = cBlock
                      , _tableDefIndices  = indexDefs
                      , _tableDefDeriving = fromMaybe [] deriv
                      }


-- | Parse list of table definitions
model :: Parser [ TableDef ]
model = do
    whitespace
    many1 table


parseTables :: String -> Either ParseError [ TableDef ]
parseTables = parse model ""
